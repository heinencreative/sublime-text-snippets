# Sublime Text 2 Snippets #

This repository contains some of my favorite snippets I use in Sublime Test. Some are for php and js. The css ones are meant to standardize commenting. The triggers for the snippets are:

* **"clog"** - console.log("breakpoint") for js debugging
* **"prepreintr"** - wraps a print_r with <pre> tags
* **"cmntIntro"** - used at start of css file to list stylesheet name and author 
* **"cmntHdr"** - use to declare big sections like "Base Styles" or "Layout Styles"
* **"cmntSubhdr"** - comment sub headers lives under headers. For example under "Layout Styles" you could have subheads of "Header" ,"Main Content" or "Footer".
* **"#"** - outputs <% Code %>
* **"#="** - outputs <%= Code %>
* **"#cmnt"** - outputs <%# Comment %>
* **"#if"** - create an if statement <% if Conditional %>...<% end%>
* **"#ife"** - create an if else statement <% if Conditional %>...<% else %>...<% end%>

## Installation ##

To install these snippets, grab the **.sublime-snippets** files you need and add them to:

```
#!bash
/Users/YOU/Library/Application Support/Sublime Text 2/Packages/User/

```